#!/usr/bin/python3
#
# Copyright (C) 2019 Daniel P. Berrange
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import argparse
import subprocess

parser = argparse.ArgumentParser(description="Bulk image rename")
parser.add_argument("--camera", dest="camera", help="Camera name")
parser.add_argument("--name", dest="name", help="Session/photoshoot name")
parser.add_argument("-n", default=False, dest="nop", action="store_true", help="Print changes only, don't rename")

args = parser.parse_args()

camera = args.camera
name = args.name

here = os.getcwd()
basedir, reldir = os.path.split(here)

suffix = ""
if reldir in ["Lights", "Darks", "Flats", "DarkFlats", "Bias"]:
    if reldir != "Lights":
        suffix = "-" + reldir
    basedir, reldir = os.path.split(basedir)

if name is None:
    name = reldir

fnames = sorted(os.listdir("."), key=lambda s: s.lower())

prefixes = []
files = {}

cameras = {
    "NIKON 1 J3": "Digital-Nikon-J3",
    "NIKON D90": "Digital-Nikon-D90",
    "NIKON D5100": "Digital-Nikon-D5100",
    "Canon EOS 450D": "Digital-Canon-450D",
}

def detect_camera(fname):
    exif = subprocess.Popen(["exiftool", "-p", "$Model", fname], stdout=subprocess.PIPE)
    out, err = exif.communicate()
    model = out.decode("utf8")
    model = model.strip()

    if model in cameras:
        return cameras[model]
    return None

for fname in fnames:
    base, ext = os.path.splitext(fname)
    if ext == "":
        continue
    if ext == ".xmp":
        base, ext = os.path.splitext(base)
        ext = ext + ".xmp"

    if camera is None:
        camera = detect_camera(fname)
        if camera is None:
            print("Unable to detect camera model")
            sys.exit(1)

    if base not in files:
        files[base] = []
        prefixes.append(base)

    files[base].append(ext)

digits = 0
nfiles = len(prefixes)
while nfiles > 0:
    digits = digits + 1
    nfiles = int(nfiles / 10)

if digits < 3:
    digits = 3

count = 1
for prefix in prefixes:

    for ext in files[prefix]:
        fmt = "%s-%s%s-%0" + str(digits) + "d%s"

        lcext = ext.lower()
        src = prefix + ext
        dst = fmt % (camera, name, suffix, count, lcext)

        if src != dst:
            print ("%s -> %s" % (src, dst))
            if not args.nop:
                os.rename(src, dst)

    count = count + 1
